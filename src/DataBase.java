public class DataBase {
    private final Student[] students;
    private int length;

    public DataBase() {
        this.students = new Student[10];
        this.length = 0;
    }

    public void outputExcellentStudent(){
        int counter;
        for (int i = 0; i < 10; i++){
            counter = 0;
            for (int j = 0; j < 5; j++){
                if(students[i].getAcademicPerformance()[j] < 9){
                    break;
                } else{
                    counter++;
                }
            }
            if (counter == 5){
                System.out.println("group: " + students[i].getGroupNumber() + ", " + students[i].getSurname() + " " + students[i].getInitials()) ;
            }
        }
    }
    public void add(Student a){
        students[length] = a;
        length++;
    }
}
