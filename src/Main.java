public class Main {
    public static void main(String[] args) {
        DataBase base = new DataBase();
        base.add(new Student("Ivanov", "A. K.", 1, new int[]{7, 3, 6, 8, 9}));
        base.add(new Student("Sidorov", "J. P.", 3, new int[]{5, 7, 8, 8, 8}));
        base.add(new Student("Nietzsche", "F. W.", 4, new int[]{7, 9, 9, 8, 9}));
        base.add(new Student("Karatkevich", "U. S.", 2, new int[]{10, 9, 10, 10, 9}));
        base.add(new Student("Razanov", "A. S.", 1, new int[]{9, 9, 9, 9, 9}));
        base.add(new Student("Egorov", "N. O.", 4, new int[]{8, 8, 8, 8, 8}));
        base.add(new Student("Smith", "A. G.", 3, new int[]{9, 6, 8, 10, 9}));
        base.add(new Student("Harms", "D. I.", 5, new int[]{5, 3, 6, 2, 9}));
        base.add(new Student("Straltsov", "M. L.", 2, new int[]{10, 10, 9, 10, 10}));
        base.add(new Student("Mozart", "W. A.", 5, new int[]{9, 9, 10, 9, 9}));

        base.outputExcellentStudent();
    }
}