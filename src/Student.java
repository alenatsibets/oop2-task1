import java.util.Arrays;

public class Student {
    private final String surname;
    private final String initials;
    private final int groupNumber;
    private final int[] academicPerformance;

    public Student(String surname, String initials, int groupNumber, int[] academicPerformance) {
        this.surname = surname;
        this.initials = initials;
        this.groupNumber = groupNumber;
        this.academicPerformance = academicPerformance;
    }

    public int[] getAcademicPerformance() {
        return academicPerformance;
    }

    public String getSurname() {
        return surname;
    }

    public String getInitials() {
        return initials;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    @Override
    public String toString() {
        return "Student{" +
                "surname='" + surname + '\'' +
                ", initials='" + initials + '\'' +
                ", groupNumber=" + groupNumber +
                ", academicPerformance=" + Arrays.toString(academicPerformance) +
                '}';
    }
}
